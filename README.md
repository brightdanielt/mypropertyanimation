What's this?
=============
Ａ simple practice following [Advanced Android in Kotlin 03.1:Property Animation] in CodeLabs.

Thoughts after class
=============
The class is simple but good, there are easy-to-understand schematics,
<br>sometimes a chart is better than fifty words explaining because the former tells the true
<br>and the latter requires more logic connecting and makes more possibility,
<br>but which way to use? It depends 🤓

Thanks Android CodeLabs ☘️☘️☘️

[Advanced Android in Kotlin 03.1:Property Animation]:https://developer.android.com/codelabs/advanced-android-kotlin-training-property-animation#0